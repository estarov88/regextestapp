﻿using RegExTestApp.Core;
using RegExTestApp.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExTestApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var parser = new WebParser(new CsvFileSaver());
            var urls = GetUrls();

            await parser.ExecuteAsync(GetWorldcatSettings(), urls);

            await parser.ExecuteAsync(GetCatalogSettings(), urls);

            await parser.ExecuteAsync(GetGoogleSettings(), urls);
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        static List<string> GetUrls()
        {
            List<string> urls = null;

            if (false)
            {
                urls = new List<string>
                {
                    @"https://lccn.loc.gov/2017593384/mods",
                    @"https://www.worldcat.org/title/early-history-of-thurston-county-washington/oclc/5692696519&referer=brief_results",
                    @"https://www.worldcat.org/title/elementos-meteorologicos-e-climatologicos/oclc/38224736&referer=brief_results",
                    @"https://www.worldcat.org/title/lte-security/oclc/796758750",
                    @"https://www.worldcat.org/title/go-programming-language-advanced-topics-in-go-golang/oclc/1111634288",
                    @"https://catalog.loc.gov/vwebv/staffView?searchId=16303&recPointer=5&recCount=25&bibId=19928096",
                    @"https://catalog.loc.gov/vwebv/staffView?searchId=24007&recPointer=0&recCount=25&bibId=19579252",
                    @"https://catalog.loc.gov/vwebv/staffView?searchId=16332&recPointer=5&recCount=25&bibId=12530111",
                    @"https://books.google.com.ua/books?id=A00RkAEACAAJ&newbks=0&hl=en&redir_esc=y",
                    @"https://books.google.com.ua/books?id=0klsAQAAQBAJ&newbks=0&hl=en&redir_esc=y",
                    @"https://books.google.com.ua/books?id=fhpYTbos8OkC&newbks=0&hl=en&redir_esc=y",
                    @"https://www.worldcat.org/title/seattle-post-intelligencer/oclc/9563195&referer=brief_results",
                    @"https://www.worldcat.org/title/men-of-the-pacific-coast-containing-portraits-and-biographies-of-the-professional-financial-and-business-men-of-california-oregon-and-washington-1902-1903/oclc/16691779&referer=brief_results",
                    @"https://www.worldcat.org/title/climate-of-africa/oclc/421441901&referer=brief_results",
                    @"https://www.worldcat.org/title/pacific-northwest-quarterly/oclc/02392232",
                    @"https://www.worldcat.org/title/seattle-times/oclc/439422874&referer=brief_results",
                    @"https://www.worldcat.org/title/tacoma-its-history-and-its-builders-a-half-century-of-activity/oclc/656923949&referer=brief_results",
                    @"https://www.worldcat.org/title/raskidanae-gnazdo-drama-u-5-eh-aktah/oclc/490947606&referer=brief_results",
                    @"https://lccn.loc.gov/95109999/mods",
                    @"https://lccn.loc.gov/rc01000565/mods",
                    @"https://lccn.loc.gov/2017593384/mods"
                };
            }
            else
            {
                try
                {
                    StreamReader sr = new StreamReader(@"URL2.csv");
                    urls = new List<string>(50);

                    string strData = sr.ReadLine();
                    while ((strData = sr.ReadLine()) != null)
                    {
                        urls.AddRange(strData.Split(';', StringSplitOptions.None));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }

            

            return NormalizeUrls(urls);
        }

        static List<string> NormalizeUrls(List<string> urls)
        {
            if (urls == null) return urls;

            var googleUrlPattern = GetGoogleSettings().UrlPattern;
            var catalogUrlPattern = GetCatalogSettings().UrlPattern;

            for (int i = 0; i < urls.Count; i++)
            {
                if (urls[i].StartsWith(googleUrlPattern))
                {
                    var regexResult = Regex.Match(urls[i], @"(id=[\w\d-]+)&");
                    if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count == 2)
                    {
                        urls[i] = $@"{googleUrlPattern}?{regexResult.Groups[1].Value}&newbks=0&hl=en&redir_esc=y";
                    }
                }
                else
                {
                    if (urls[i].StartsWith(catalogUrlPattern))
                    {
                        var regexResult = Regex.Match(urls[i], $@"{catalogUrlPattern}/([\w\d-]+)[/]?");
                        if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count == 2)
                        {
                            urls[i] = $@"{catalogUrlPattern}/{regexResult.Groups[1].Value}/mods";
                        }
                    }
                    else
                    {
                        if (urls[i].StartsWith(@"https://catalog.loc.gov"))
                        {
                            var regexResult = Regex.Match(urls[i], @"searchArg=([\w\d-]+)&");
                            if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count == 2)
                            {
                                urls[i] = $@"{catalogUrlPattern}/{regexResult.Groups[1].Value}/mods";
                            }
                        }
                    }
                }
            }

            return urls;
        }

        static ParserSettings GetWorldcatSettings()
        {
            string urlPattern = "https://www.worldcat.org/title";

            return new ParserSettings(urlPattern, "BooksWorldcatParser")
            { HtmlFilterPattern = @"id=""record-cont""([\w\W]*?)id=""req_confirmation""" }
                .AddPattern(FieldNames.Author, new string[]
                {
                    @"<a[\W\w]*?qt=hot_author[\W\w]+?>([\w\W]+?)<",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.Editor, null)
                .AddPattern(FieldNames.Translator, null)
                .AddPattern(FieldNames.ISBN, new string[] { @"id=""details-standardno""[\W\w]*?<td>([\w\W]+?)<" })
                .AddPattern(FieldNames.LCCN, null)
                .AddPattern(FieldNames.OCLC, new string[] { @"id=""details-(?:oclcno|unique-id)""[\W\w]*?<td>([\w\W]+?)<" })
                .AddPattern(FieldNames.ISSN, new string[] { @"ISSN:[\W\w]*?<td>([\d-]+)" })
                .AddPattern(FieldNames.PublishDate, new string[] { @"id=""bib-publisher-cell""[\W\w]{1,200}([\d]{4,4})[\W\w]{0,3}<" })
                .AddPattern(FieldNames.PublisherLocation, new string[] { @"id=""bib-publisher-cell""[\W\w]*?>([\w\W]+?)[,|:|<]" })
                .AddPattern(FieldNames.PublisherName, new string[] { @"id=""bib-publisher-cell""[\W\w]*?>[\W\w]*?[:]([\w\W]+?)[<]" })
                .AddPattern(FieldNames.Title, new string[] { @"id=""bibdata""[\W\w]*?<h1[\W\w]+?>([\w\W]+?)[/|<]" })     
                .AddPattern(FieldNames.TitleTranslit, new string[] { @"id=""bibdata""[\W\w]*?<h1[\W\w]+?[/]([\w\W]+?)</h1>" });     
        }

        static ParserSettings GetCatalogSettings()
        {
            string urlPattern = "https://lccn.loc.gov";

            return new ParserSettings(urlPattern, "BooksCatalogParser")
            { HtmlFilterPattern = null }
                .AddPattern(FieldNames.Author, new string[]
                {
                    @"<name type=""personal"" usage=""primary""[\w\W^>]*?><namePart>([\w\W]+?)</namePart>",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.Editor, new string[]
                {
                    @"<name type=""personal""><namePart>([\w\W]+?)</namePart>",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.Translator, new string[]
                {
                    @"<name type=""personal""><namePart>([\w\W]+?)</namePart><role><roleTerm type=""text"">translator</roleTerm>",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.ISBN, new string[] { @"<identifier type=""isbn"">([\w\W]+?)</identifier>" })
                .AddPattern(FieldNames.LCCN, new string[] { @"<identifier type=""lccn"">([\w\W]+?)</identifier>" })
                .AddPattern(FieldNames.OCLC, new string[] { @"<identifier type=""oclc"">[\D0]*([\d]+)</identifier>" })
                .AddPattern(FieldNames.ISSN, new string[] { @"<identifier type=""issn-l"">([\w\W]+?)</identifier>" })
                .AddPattern(FieldNames.PublishDate, new string[] { @"<dateIssued[\w\W^>]*?>[\w\W]*?([\d]{4,4})[\w\W]*?</dateIssued>" })
                .AddPattern(FieldNames.PublisherLocation, new string[] { @"<place[\w\W^>]*?><placeTerm type=""text"">([\w\W]+?)[<|,]" })
                .AddPattern(FieldNames.PublisherName, new string[] { @"<publisher[\w\W^>]*?>([\w\W]+?)</publisher>" })
                .AddPattern(FieldNames.Title, new string[] { @"<titleInfo>([\w\W]+?)</title>" })
                .AddPattern(FieldNames.TitleTranslit, new string[] { @"<titleInfo type=""translated"">([\w\W]+?)</title>" });
        }

        static ParserSettings GetGoogleSettings()
        {
            string urlPattern = "https://books.google.com.ua/books";

            return new ParserSettings(urlPattern, "BooksGoogleParser")
            { HtmlFilterPattern = @"<h3 class=""about_title"">Bibliographic information([\w\W]+)" }
                .AddPattern(FieldNames.Author, new string[]
                {
                    @">[\s]?Author[\W\w]+?q=inauthor[\W\w]+?>([\w\W]+?)</span></a></td></tr>",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.Editor, new string[]
                {
                    @">[\s]?Editor[\W\w]+?q=inauthor[\W\w]+?>([\w\W]+?)</span></a></td></tr>",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.Translator, new string[]
                {
                    @">[\s]?Translate[\W\w]{20,30}metadata_value"">(?:<span dir=ltr>)?([\w\W]+?)[<]",
                    @"^(?:Mr[\s\.]+|MR[\s\.]+|mr[\s\.]+|Dr[\s\.]+|DR[\s\.]+|dr[\s\.]+|Sir[\s\.]+|SIR[\s\.]+|sir[\s\.]+)([\w\W]+)"
                })
                .AddPattern(FieldNames.ISBN, new string[] { @">[\s]?ISBN[\W\w]{20,30}metadata_value"">(?:<span dir=ltr>)*([\w\W]+?)[<]" })
                .AddPattern(FieldNames.LCCN, null)
                .AddPattern(FieldNames.OCLC, null)
                .AddPattern(FieldNames.ISSN, null)
                .AddPattern(FieldNames.PublishDate, new string[] { @">[\s]?Publish[\W\w]{20,30}metadata_value"">(?:<span dir=ltr>)?[\w\W]{0,100}?([\d]{4})[\.|\s|<]" })
                .AddPattern(FieldNames.PublisherLocation, null)
                .AddPattern(FieldNames.PublisherName, new string[] { @">[\s]?Publish[\W\w]{20,30}metadata_value"">(?:<span dir=ltr>)?([\w\W]{0,100}?)(?:19\d{2}|20\d{2})?[\s]?[\.|<]" })
                .AddPattern(FieldNames.Title, new string[] { @">[\s]?Title[\W\w]{10,20}metadata_value"">(?:<span dir=ltr>)?([\w\W]+?)[<]" });
        }
    }
}
