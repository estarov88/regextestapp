﻿using RegExTestApp.Core.Models;

namespace RegExTestApp.Core.Interfaces
{
    public interface ISaver
    {
        bool Save(ParserResult parseData, string parserName);
    }
}
