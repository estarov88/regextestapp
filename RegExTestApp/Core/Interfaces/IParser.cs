﻿using RegExTestApp.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegExTestApp.Core.Interfaces
{
    public interface IParser
    {
        ParserResult Result { get; }
        bool IsSuccess { get; }

        Task ExecuteAsync(ParserSettings settings, IEnumerable<string> urls);
        bool Save(ISaver saver);
    }
}
