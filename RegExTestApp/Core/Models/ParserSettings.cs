﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RegExTestApp.Core.Models
{
    public class ParserSettings
    {
        private readonly Dictionary<FieldNames, IEnumerable<string>> _patterns;
        public string ParserName { get; private set; }
        public string UrlPattern { get; private set; }
        public string HtmlFilterPattern { get;  set; }
        public ParserSettings(string urlPattern, string parserName)
        {
            UrlPattern = urlPattern ?? throw new ArgumentException("urlPattern required");
            ParserName = parserName ?? "WebParser";
            var keys = Enum.GetValues(typeof(FieldNames));
            _patterns = new Dictionary<FieldNames, IEnumerable<string>>(keys.Length);
            foreach (FieldNames item in keys)
            {
                _patterns.Add(item, null);
            }
        }

        public ParserSettings AddPattern(FieldNames key, IEnumerable<string> patterns)
        {
            _patterns[key] = patterns;
            return this;
        }

        public Dictionary<FieldNames, IEnumerable<string>> GetPatterns()
        {
            return new Dictionary<FieldNames, IEnumerable<string>>(_patterns.Where(p => p.Value != null && p.Value.Any()));
        }
    }
}
