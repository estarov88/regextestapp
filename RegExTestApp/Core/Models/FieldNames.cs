﻿namespace RegExTestApp.Core.Models
{
    public enum FieldNames
    {
        Title,
        TitleTranslit,
        PublisherLocation,
        PublisherName,
        PublishDate,
        Author,
        Editor,
        Translator,
        ISBN,
        OCLC,
        LCCN,
        ISSN,
        SourceUrl
    }
}
