﻿using System;
using System.Collections.Generic;

namespace RegExTestApp.Core.Models
{
    public class ParserResult
    {
        public List<Dictionary<FieldNames, string>> Map { get; private set; }
        public ParserResult()
        {
            Map = new List<Dictionary<FieldNames, string>>();
        }

        public void AddData(Dictionary<FieldNames, string> data)
        {
            if(data == null) return;
            var normalizeData = GetEmptyDataDictionary();
            foreach (var item in data)
            {
                normalizeData[item.Key] = item.Value;
            }
            Map.Add(normalizeData);
        }

        public static Dictionary<FieldNames, string> GetEmptyDataDictionary()
        {
            var keys = Enum.GetValues(typeof(FieldNames));
            var data = new Dictionary<FieldNames, string>(keys.Length);
            foreach (FieldNames item in keys)
            {
                data.Add(item, "");
            }
            return data;
        }
    }
}
