﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Io;
using RegExTestApp.Core.Extensions;
using RegExTestApp.Core.Interfaces;
using RegExTestApp.Core.Models;

namespace RegExTestApp.Core
{
    public class WebParser : IParser
    {
        private bool _saveDone = true;
        private readonly IBrowsingContext _context;
        protected IEnumerable<string> Urls { get; set; }
        protected ParserSettings Settings { get; set; }
        protected ISaver Saver { get; set; }
        protected IDocument Document { get; set; }
        public ParserResult Result { get; protected set; }
         
        public bool IsSuccess
        {
            get
            {
                return (Result != null && _saveDone);
            }
        }

        public WebParser(ISaver saver = null)
        {
            Saver = saver;

            var config = Configuration.Default
               .WithDefaultLoader(new LoaderOptions
               {
                   IsResourceLoadingEnabled = true,
                   IsNavigationDisabled = false
               }).WithJs();

            _context = BrowsingContext.New(config);
        }

        protected virtual async Task<ParserResult> Parse()
        {
            Result = new ParserResult();

            foreach (var url in Urls)
            {
                string html = GetHtml(url);
                if (!string.IsNullOrWhiteSpace(html))
                {
                    var data = ParserResult.GetEmptyDataDictionary();
                    data[FieldNames.SourceUrl] = url;
                    foreach (var item in Settings.GetPatterns())
                    {
                        switch (item.Key)
                        {

                            case FieldNames.Author:
                                data[item.Key] = string.Join("|", GetNames(html, item.Value).Select(n => n.FullName));
                                break;
                            case FieldNames.Editor:
                                data[item.Key] = string.Join("|", GetNames(html, item.Value).Select(n => n.FullName));
                                break;
                            case FieldNames.Translator:
                                data[item.Key] = string.Join("|", GetNames(html, item.Value).Select(n => n.FullName));
                                break;
                            case FieldNames.ISBN:
                                data[item.Key] = GetISBN(html, item.Value);
                                break;
                            case FieldNames.LCCN:
                                data[item.Key] = GetValue(html, item.Value);
                                if(data[item.Key] != null)
                                {
                                    data[item.Key] = data[item.Key].Replace(" ", string.Empty);
                                }
                                break;
                            case FieldNames.SourceUrl:
                                break;
                            default:
                                data[item.Key] = GetValue(html, item.Value);
                                break;
                        }
                    }
                    Result.AddData(data);

                    Console.WriteLine();
                    Console.WriteLine($"***** Book info *****");
                    Console.WriteLine();
                    foreach (var item in data)
                    {
                        WriteToConsole($"{item.Key}: {item.Value}");
                    }
                }
            }
            
            return Result;
        }

        public async Task ExecuteAsync(ParserSettings settings, IEnumerable<string> urls)
        {
            Settings = settings ?? throw new ArgumentException("settings required");
            if(urls == null)
                throw new ArgumentException("urls required");
            Urls = urls.Where(u => u.StartsWith(Settings.UrlPattern));
            
            Console.WriteLine();
            Console.WriteLine($"***** { Settings.ParserName }. Parsing start... *****");
            Console.WriteLine();
            Result = null;
            Result = await Parse();
            Console.WriteLine();
            Console.WriteLine($"***** { Settings.ParserName }. Parsing finish. *****");
            Console.WriteLine();

            if (Saver != null && Result != null)
            {
                _saveDone = Saver.Save(Result, Settings.ParserName);
                Console.WriteLine($"{ Settings.ParserName }. Saving - { _saveDone }.");
                Console.WriteLine();
            } 
        }

        protected IEnumerable<Name> GetNames(string html, IEnumerable<string> patterns)
        {
            string[] names = null;
            string pattern = patterns?.FirstOrDefault();
            if (html != null && pattern != null)
            {
                var regexResult = Regex.Matches(html, pattern);
                if (regexResult.Count > 0)
                {
                    names = regexResult
                        .Where(m => m.Success && m.Groups != null && m.Groups.Count > 1)
                        .Select(m => m.Groups[1].Value.TrimHTML().ReplaceAll()).Distinct().ToArray();
                    if(names.Count() == 1)
                    {
                        var tmp = names.First().Split(",");
                        if(tmp.Length > 0)
                        {
                            if(tmp[0].Split(" ").Length > 1)
                            {
                                names = tmp;
                            }
                        }
                    }
                }
            }

            if(names == null)
            {
                return Array.Empty<Name>();
            }

            foreach (var item in patterns.Skip(1))
            {
                for (int i = 0; i < names.Length; i++)
                {
                    if (!string.IsNullOrWhiteSpace(names[i]))
                    {
                        var regexResult = Regex.Match(names[i], item);
                        if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count > 1)
                        {
                            names[i] = regexResult.Groups[1].Value;
                        }
                    }
                }
            }

            var result = new List<Name>(names.Length);
            foreach (var name in names)
            {
                var regexResult = Regex.Match(name, @"^([\w\W^\s]+?)[\s|,]([\w\W]+)$");
                if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count > 2)
                {
                    result.Add(new Name()
                    {
                        FirstName = regexResult.Groups[1].Value,
                        LastName = regexResult.Groups[2].Value
                    });
                }
            }

            return result;
        }

        protected string GetISBN(string html, IEnumerable<string> patterns)
        {
            string result = GetValue(html, patterns);
            if (result != null)
            {
                var isbns = Regex.Matches(result, @"[\d]+");
                if (isbns.Count > 0)
                {
                    result = isbns[0].Groups[0].Value;
                    for (int i = 1; i < isbns.Count; i++)
                    {
                        if(isbns[i].Groups[0].Value.Length < result.Length)
                        {
                            result = isbns[i].Groups[0].Value;
                        }
                    }
                }
            }

            return result ?? string.Empty;
        }

        protected string GetValue(string html, IEnumerable<string> patterns)
        {
            string result = null;
            
            if (html != null)
            {
                string source = html;
                foreach (var pattern in patterns)
                {
                    if (!string.IsNullOrWhiteSpace(pattern))
                    {
                        var regexResult = Regex.Match(source, pattern);
                        if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count > 1)
                        {
                            result = regexResult.Groups[1].Value.TrimHTML().ReplaceAll();
                            source = result;
                        }
                    }
                }
            }

            return result ?? string.Empty;
        }

        protected async Task<IDocument> LoadDocumentAsync(string url)
        {
            try
            {
                Document = await _context.OpenAsync(url).WhenStable();
                    //.Then(d => d.Load(url));
                if (Document == null)
                {
                    return null;
                }
                for (int i = 0; i < 10 && string.IsNullOrWhiteSpace(Document.QuerySelector("body").InnerHtml); i++)
                {
                    Thread.Sleep(500);
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw ex;
            }

            return Document;
        }

        protected string GetHtml(string url)
        {
            string result = null;
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = "GET";
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();

                Stream receiveStream = myResponse.GetResponseStream();
                StreamReader readStream = null;
                if (string.IsNullOrWhiteSpace(myResponse.CharacterSet))
                {
                    readStream = new StreamReader(receiveStream, Encoding.UTF8);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(myResponse.CharacterSet));
                }
                result = readStream.ReadToEnd();
                readStream.Close();

                if (myResponse.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(result);
                }
                myResponse.Close();
                //Console.WriteLine(result);
                if (!string.IsNullOrWhiteSpace(Settings.HtmlFilterPattern))
                {
                    var regexResult = Regex.Match(result, Settings.HtmlFilterPattern);
                    if (regexResult.Success && regexResult.Groups != null && regexResult.Groups.Count > 1)
                    {
                        result = regexResult.Groups[1].Value;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"GetHtml error: {ex.Message} - URL: {url}");
                return null;
            }

            return result;
        }

        protected void WriteToConsole(string text, bool inline = false)
        {
            if (inline)
            {
                Console.Write($" // {text ?? ""}");
            }
            else
            {
                Console.WriteLine(text ?? "");
                Console.WriteLine("----------------------------------------------------------------------------");
            }
        }

        public bool Save(ISaver saver)
        {
            _saveDone = saver?.Save(Result, Settings.ParserName) ?? false;
            return _saveDone;
        }
    }
}
