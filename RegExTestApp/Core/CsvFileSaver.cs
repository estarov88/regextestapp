﻿using RegExTestApp.Core.Interfaces;
using RegExTestApp.Core.Models;
using System;
using System.IO;
using System.Text;

namespace RegExTestApp.Core
{
    public class CsvFileSaver : ISaver
    {
        const string _mainFolder = "Results";

        public CsvFileSaver() { }

        public bool Save(ParserResult parseData, string parserName)
        {
            if (string.IsNullOrEmpty(parserName))
                parserName = "Default";

            if (parseData != null && parseData.Map?.Count > 0)
            {
                string dirPath = $"{Directory.GetCurrentDirectory()}\\{_mainFolder}\\{parserName}";
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                string fileName = $"{DateTime.Now.Year}_{DateTime.Now.Month}_{DateTime.Now.Day}";
                string filePath = $"{dirPath}\\{fileName}";

                for (int i = 1; i < 10000; i++)
                {
                    var path = $"{filePath}_{i}.csv";
                    if (!File.Exists(path))
                    {
                        filePath = path;
                        break;
                    }
                }

                using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var item in parseData.Map[0].Keys)
                        {
                            stringBuilder.Append($"{item};");
                        }
                        //stringBuilder.Remove(stringBuilder.Length - 1, 1);
                        sw.WriteLine(stringBuilder.ToString());

                        foreach (var item in parseData.Map)
                        {
                            stringBuilder = new StringBuilder();

                            foreach (var value in item.Values)
                            {
                                stringBuilder.Append($"{value.Replace(';', ',')};");
                            }
                            //stringBuilder.Remove(stringBuilder.Length - 1, 1);
                            sw.WriteLine(stringBuilder.ToString());
                        }
                        sw.Flush();
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
        }
    }
}
