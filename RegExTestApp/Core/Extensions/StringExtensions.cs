﻿using System.Text.RegularExpressions;

namespace RegExTestApp.Core.Extensions
{
    public static class StringExtensions
    {
        public static string TrimHTML(this string input)
        {
            return Regex.Replace(input, "<.*?>", string.Empty);
        }

        public static string ReplaceAll(this string input)
        {
            string result = Regex.Replace(input, @"^[\W]*", string.Empty);
            result = Regex.Replace(result, @"[\W]*$", string.Empty);
            result = Regex.Replace(result, @"[\[\]]", string.Empty);
            result = Regex.Replace(result, @"[\s]{2,}", " ");
            result = Regex.Replace(result, @"\b[-]\b", '\x2013'.ToString());
            return Regex.Replace(result, @"\s:", ":");
        }
    }
}
